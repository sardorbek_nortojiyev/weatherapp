package com.example.weatherapp.cache

import android.content.SharedPreferences

object PreferenceHelper {

    private inline fun SharedPreferences.edit(operation: (SharedPreferences.Editor) -> Unit){
        val editor = this.edit()
        operation(editor)
        editor.apply()
    }
}